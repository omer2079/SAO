# TODO List

# Solution
Decide
	if Server is "Server Computer" and Server Application is the "Progran"
	Or Machine is "Server Computer" and Server is the "Program"?
Create a presentation Website for Developers
	and give at a name.
	Create a new Project, but not in the Root Solution Folder
Mono-Repo (git)
git branch names format
To use Build Configuration Tool - For exmple "CMake" or "Premake"

# Development Tools

## Visual Studio Extension
New Item
New Folder
New Class

# Documents
Architecture Diagram

# Engine
Entry Point
Minimal Application Layer
Define Data Types
	Primitive
	Data Structures

Log System
	namespase Debug\Logger
	Logger Class
		Format Abillity
		Log Typs (Levels)
			Critical - Red, 1
			Error - Red, Level 2
			Warning - Yellow, Level 3
			Info - Green, Level 4: Inform about event / status changed.
				For example: Initialization end.
			
			Trace - White, Level 4: Log for Develpers.
				Debug-type message with number (count) to know the way that program moved.
			Debug - Blue, Level 4: Log for Develpers.
	
	Console Class - can out to Console, File, Other Computer
	Engine Macros - ENGINE_LOG_ERROR ...
	Application Macros - APP_LOG_ERROR ...

# Engine

...
Application Layer
Window Layer
	Input
	Events
Renderer
Render API Abstraction
Debugging Support
Scripting Language
Memory Systems
Entity - Component System (ECS)
Physics
File IO - VFS
Build System