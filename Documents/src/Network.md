namespace Network
{
	abstract class Socket
	{
		Port
	}

	class ServerSocket : Socket
	{
		Clients
		abstract class Client
		{
			IP

			Disconnect()

			Events:
			DataReceived
		}

		Open()
		Listen()
		Close()
		IsOpened()

		Events: Client Connected
	}

	class ClientSocket : Socket
	{
		ServerIP
		Connect()
		Disconnect()
		IsConnected()
		SendData()
		
		Events: Connected, Disconnected, DataResived
	}
	
	class IP
	class Ping
	class DNS
}