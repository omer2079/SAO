class Object abstract
{
public:
	virtual ~Object() { };

	virtual String ToString() const = 0;
};