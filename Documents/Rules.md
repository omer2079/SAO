# Rules

# Global Names
Engine - The Game Engine / Core Engine
Application - The Game / Client program
Server - Program that runs on the Back-End and communicate through the network.
Website - Client program that runs on Web browsers
Logger - Console or API of the logging system.
Entity - Object that can be in the 3D World.

src - Source Code Folder

# Build

Configurations:

Release - (Production, PRODUCTION)
	Final optimal performance and for release the application
	* Debug namespace does not exist, the macros are equal to blank code.
Test - (Lab Testing, TEST)
	Optimal performance with console (logger).
	* Trace-log and Debug-log does not exist, their macros are equal to blank code.
Debug - (Develop, DEBUG)

Platforms:

Windows - (x64)
	Just 64 Bit, it is Can't be on the 32 Bit computers.

# Input / Output (I/O)

Inputs:

Keyboard - (Keys)
Mouse - (Movement, Buttons, Scroll Wheel)
Microphone
Camera

VR (Virtual Reality) - Unknown

Outputs:

Computer Monitor(s)
Speaker
Headset

# IDEs Support
Visual Studio
Visual Studio Code

# Code
# Namespaces
* Each Namespace in a separate folder
  Folder path = namespace path

# Header Files

* Include the "Minimal.h" header.

# Data Types
# Primitive Data Types

Not need to define:
	bool, char, short, int, float, double

Need to define:
	Byte - unsigned char, size: 1 bytes
	ushort - unsigned short, size: 2 bytes
	uint - unsigned int, size: 4 bytes
	Long - long long, size: 8 bytes
	Ulong - unsigned long long, size: 8 bytes

# Object class

The root class of all the classes (that not static) in the Engine

# Data Structures

String - Array of chars
Function

Templates:
	Pointers
	Collections

# Classes

All the classes must be based on the Object or IObject classes

* Classes must to be define by the 'class', And never by the 'struct' keyword.
* Public before protected and protected before private
* Each class (not a sub-class) in a separate file
* Each member Variable in a separate line

# Class Structure Order

1. static Variables
2. static Functions

3. Variables
4. Constructor and Destructor
5. Functions

6. Sub-classes

# Enums
* Each Enum in a separate file